export interface IEmployee {
  id: string;
  firstname: string;
  lastname: string;
  email: string;
  website: string;
  phone: string;
}
