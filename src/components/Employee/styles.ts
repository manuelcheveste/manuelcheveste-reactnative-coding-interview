import { StyleSheet, Platform } from 'react-native';

const styles = {
  employeeInfo:
    Platform.OS === 'android'
      ? {
          borderBottomWidth: 1,
          borderLeftWidth: 1,
          borderRightWidth: 1,
          borderTopWidth: 1,
          padding: 10,
          width: '90%',
          borderRadius: 10,
        }
      : {},
  employeeItem:
    Platform.OS === 'android'
      ? {
          justifyContent: 'center',
        }
      : {},
};

export default StyleSheet.create({
  employeeItem: {
    flexDirection: 'row',
    padding: 10,
    paddingVertical: 15,
    ...styles.employeeItem,
  },
  employeeAvatar: {
    height: 48,
    width: 48,
    borderWidth: 1,
    borderRadius: 100,
    marginHorizontal: 10,
  },
  employeeInfo: {
    justifyContent: 'space-around',
    ...styles.employeeInfo,
  },
});
