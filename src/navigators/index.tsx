import React from 'react';
import { Text } from 'react-native';

import { createDrawerNavigator } from '@react-navigation/drawer';
import { EmployeeDetailScreen, EmployeesScreen } from '../screens';
import { ScreenParamsList } from './paramsList';

const Drawer = createDrawerNavigator();

export function MainNavigator() {
  return (
    <Drawer.Navigator
      // screenOptions={{
      //   headerTintColor: '#6C3ECD',
      //   headerTitleAlign: 'center',
      // }}
      drawerContent={() => <Text>Profile</Text>}>
      <Drawer.Screen component={EmployeesScreen} name="Employees" />
      <Drawer.Screen component={EmployeeDetailScreen} name="EmployeeDetail" />
    </Drawer.Navigator>
  );
}
